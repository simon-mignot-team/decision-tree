//
// Created by Simon on 22/11/2017.
//

#ifndef EXO1_TREE_H
#define EXO1_TREE_H

#include <iostream>
#include <vector>
#include <map>

enum NodeChild { NotLastChild = 0, LastChild};

struct Node
{
    Node* parent;
    int idColumn;
    int idValue;
    int idColumnNext;
    std::vector<Node*> children;
    std::vector<std::map<int, int>> data;
    bool value;

    void display();
};


class Tree
{
    public:
        Tree();

        static void displayTree(Node* root, std::vector<NodeChild> depths = std::vector<NodeChild>());

    private:
        static void writeDepthTree(std::vector<NodeChild> depths = std::vector<NodeChild>());

        Node root;
};


#endif //EXO1_TREE_H
