//
// Created by septimus on 26/11/17.
//

#include "Calculation.h"

// yes & no : number of times
// the function calculates the frequency
double Calculation::entropy(double yes, double no)
{
    double total = yes + no;
    double result = 0;
    yes /= total;
    no /= total;
    if(yes > 0)
        result -= yes * std::log2(yes);
    if(no > 0)
        result -= no * std::log2(no);
    return result;
}

double Calculation::pertinence(std::map<int, std::pair<int, int>> values)
{
    double result = 1;
    double total = sumColumn(values);
    for(std::map<int, std::pair<int, int>>::iterator it = values.begin(); it != values.end(); ++it)
    {
        double entropy = Calculation::entropy(it->second.first, it->second.second);
        result += -((it->second.first * entropy) / total) - ((it->second.second * entropy) / total);
    }
    return result;
}

int Calculation::sumColumn(std::map<int, std::pair<int, int>> values)
{
    int result = 0;
    for(std::map<int, std::pair<int, int>>::iterator it = values.begin(); it != values.end(); ++it)
        result += it->second.first + it->second.second;
    return result;
}

