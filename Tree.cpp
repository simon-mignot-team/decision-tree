//
// Created by Simon on 22/11/2017.
//

#include "Tree.h"

#include <typeinfo>

Tree::Tree()
{

}

void Node::display()
{
    /*if(parent != NULL)
    {
        for(int i = 0; i < depth; ++i)
            std::cout << "│   ";
    }*/
    if(!children.empty())
    {
        char c = (idColumn != -1) ? static_cast<char>(idColumn + 48) : '.';
        char v = (idValue != -1) ? static_cast<char>(idValue + 48) : '.';
        std::cout << c << v;
    }
    else
        std::cout << (value ? "YES" : "NO");
    std::cout << '\n';
}

void Tree::displayTree(Node* root, std::vector<NodeChild> depths)
{
    Tree::writeDepthTree(depths);
    root->display();
    for(int i = 0; i < root->children.size(); ++i)
    {
        if(i < root->children.size() - 1)
            depths.push_back(NodeChild::NotLastChild);
        else
            depths.push_back(NodeChild::LastChild);
        Tree::displayTree(root->children[i], depths);
        depths.pop_back();
    }
}

void Tree::writeDepthTree(std::vector<NodeChild> depths)
{
    for(int i = 0; i < depths.size(); ++i)
    {
        // if last depth
        if(i == depths.size() - 1)
        {
            if(depths[i] == NodeChild::NotLastChild)
                std::cout << "├───";
            else if(depths[i] == NodeChild::LastChild)
                std::cout << "╰───";
        }
        else
        {
            if(depths[i] == NodeChild::NotLastChild)
                std::cout << "│   ";
            else if(depths[i] == NodeChild::LastChild)
                std::cout << "    ";
        }
    }
}
