#include <iostream>
#include <vector>
#include "Decision.h"

namespace Sky {
    const int Sky = 0;
    enum { SUN = 0, CLOUDY, RAIN }; };
namespace Temperature {
    const int Temperature = 1;
    enum { WARM = 0, SOFT, COLD }; };
namespace Humidity {
    const int Humidity = 2;
    enum { HIGH = 0, LOW }; };
namespace Wind {
    const int Wind = 3;
    enum { HIGH = 0, LOW }; };
namespace Target {
    const int Target = 4; };

int main()
{
    std::vector<std::map<int, int>> data;
    data.push_back({ { Sky::Sky, Sky::SUN }, { Temperature::Temperature, Temperature::WARM },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::LOW }, { Target::Target, 0 } });
    data.push_back({ { Sky::Sky, Sky::SUN }, { Temperature::Temperature, Temperature::WARM },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::HIGH }, { Target::Target, 0 } });
    data.push_back({ { Sky::Sky, Sky::CLOUDY }, { Temperature::Temperature, Temperature::WARM },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::LOW }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::RAIN }, { Temperature::Temperature, Temperature::SOFT },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::LOW }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::RAIN }, { Temperature::Temperature, Temperature::COLD },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::LOW }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::RAIN }, { Temperature::Temperature, Temperature::COLD },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::HIGH }, { Target::Target, 0 } });
    data.push_back({ { Sky::Sky, Sky::CLOUDY }, { Temperature::Temperature, Temperature::COLD },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::HIGH }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::SUN }, { Temperature::Temperature, Temperature::SOFT },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::LOW }, { Target::Target, 0 } });
    data.push_back({ { Sky::Sky, Sky::SUN }, { Temperature::Temperature, Temperature::COLD },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::LOW }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::RAIN }, { Temperature::Temperature, Temperature::SOFT },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::LOW }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::SUN }, { Temperature::Temperature, Temperature::SOFT },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::HIGH }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::CLOUDY }, { Temperature::Temperature, Temperature::SOFT },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::HIGH }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::CLOUDY }, { Temperature::Temperature, Temperature::WARM },
                     { Humidity::Humidity, Humidity::LOW }, { Wind::Wind, Wind::LOW }, { Target::Target, 1 } });
    data.push_back({ { Sky::Sky, Sky::RAIN }, { Temperature::Temperature, Temperature::SOFT },
                     { Humidity::Humidity, Humidity::HIGH }, { Wind::Wind, Wind::HIGH }, { Target::Target, 0 } });

    Decision d;
    d.createDecisionTree(data);

    return 0;
}