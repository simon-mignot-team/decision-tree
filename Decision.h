//
// Created by Simon on 22/11/2017.
//

#ifndef EXO1_DECISION_H
#define EXO1_DECISION_H


#include <iostream>
#include <vector>
#include <map>
#include "Calculation.h"
#include "Tree.h"

class Decision
{
    public:
        Decision();
        Tree createDecisionTree(std::vector<std::map<int, int>> data);
        std::map<int, std::pair<int, int>> countYesNo(int columnID, std::vector<std::map<int, int>> data);
        void init(std::vector<std::map<int, int>> data);

    private:
        void computeData(Node* node);
        int getMostPertinentColumn(std::vector<std::map<int, int>> data);

        int m_targetColumn;
        int iteration;
};


#endif //EXO1_DECISION_H
