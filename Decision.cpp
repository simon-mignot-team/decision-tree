//
// Created by Simon on 22/11/2017.
//

#include "Decision.h"

Decision::Decision()
{

}

void Decision::init(std::vector<std::map<int, int>> data)
{
    m_targetColumn = static_cast<int>(data[0].size() - 1);
}


Tree Decision::createDecisionTree(std::vector<std::map<int, int>> data)
{
    init(data);
    iteration = 0;
    Node* root = new Node;
    root->parent = NULL;
    root->idValue = -1;
    root->data = data;

    computeData(root);


    Tree::displayTree(root);
    return Tree();
}

void displayTree(Node* node)
{
    while(node->parent != NULL)
        node = node->parent;
    Tree::displayTree(node);
}

// TODO: split the function
void Decision::computeData(Node* node)
{
    ++iteration;
    int mostPertinentColumn = getMostPertinentColumn(node->data);
    std::map<int, std::pair<int, int>> differentValues = countYesNo(mostPertinentColumn, node->data);
    double pertinence = Calculation::pertinence(differentValues);

    if(pertinence == 1 && differentValues.size() == 1)
    {
        for(int currentValue = 0; currentValue < differentValues.size(); ++currentValue)
        {
            Node* leaf = new Node;
            leaf->parent = node;
            if(differentValues[currentValue].first > 0 && differentValues[currentValue].second == 0)
                leaf->value = true;
            else if(differentValues[currentValue].first == 0 && differentValues[currentValue].second > 0)
                leaf->value = false;
            else
                std::cout << "=== ERROR\n\n\n";
            node->children.push_back(leaf);
        }
    }
    else
    {
        node->idColumn = mostPertinentColumn;
        // TODO: use map iterator
        for(int currentValue = 0; currentValue < differentValues.size(); ++currentValue)
        {
            Node* n = new Node;
            n->parent = node;
            n->idColumn = -1;
            n->idValue = currentValue;
            for(std::vector<std::map<int, int>>::iterator it = node->data.begin(); it != node->data.end();)
            {
                if((*it)[mostPertinentColumn] == currentValue)
                {
                    n->data.push_back(*it);

                    // Inverse the commented line to copy or move the data
                    it = node->data.erase(it);
                    //++it

                    n->data[n->data.size() - 1].erase(mostPertinentColumn);
                }
                else
                    ++it;
            }
            node->children.push_back(n);
            computeData(n);
        }
    }
}


std::map<int, std::pair<int, int>> Decision::countYesNo(int columnID, std::vector<std::map<int, int>> data)
{
    std::map<int, std::pair<int, int>> result;

    for(int i = 0; i < data.size(); ++i)
        if(data[i][m_targetColumn])
            ++(result[data[i][columnID]]).first;
        else
            ++(result[data[i][columnID]]).second;
    return result;
}

int Decision::getMostPertinentColumn(std::vector<std::map<int, int>> data)
{
    int mostPertinent = -1;
    double currentPertinence = 0;
    int differentValueCount = 0;

    for(int i = 0; i < m_targetColumn; ++i)
    {
        std::map<int, std::pair<int, int>> tmp = countYesNo(i, data);
        double pertinence = Calculation::pertinence(tmp);
        if(pertinence > currentPertinence)
        {
            currentPertinence = pertinence;
            mostPertinent = i;
            differentValueCount = static_cast<int>(tmp.size());
        }
    }
    return mostPertinent;
}
