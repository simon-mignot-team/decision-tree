//
// Created by septimus on 26/11/17.
//

#ifndef EXO1_CALCULATION_H
#define EXO1_CALCULATION_H

#include <iostream>
#include <vector>
#include <map>
#include <cmath>

class Calculation
{
    public:
        static double entropy(double yes, double no);
        static double pertinence(std::map<int, std::pair<int, int>> values);

    private:
        static int sumColumn(std::map<int, std::pair<int, int>> values);
};


#endif //EXO1_CALCULATION_H
